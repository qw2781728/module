@extends("layout.app")
@section('content') 
    <div class="page-header">
        <h2>
            <span class="text-muted font-weight-light">
                <span>&nbsp;&nbsp;&nbsp;&nbsp;首頁設定</span>
            </span>
        </h2>
    </div>
    <div class="container">
        {{ Form::open(['method' => 'POST', 'action' => 'HomeController@store']) }}
            <div class="row">
                <div class="col-md-6">
                    <!-- <div class="form-group">
                        <label>Logo</label>&nbsp;&nbsp;&nbsp;<b class="text-danger">建議尺寸：225＊165 pixel</b><br>
                        <img class="preview" src="https://placehold.it/300x300?text=photo1.jpg" style="margin:10px 0;"><br>
                        <input type='hidden' name="logo">
                        <input style="margin:10px 0;" type='file' name="img" accept='image/*' id="logo"><br>
                        <label class="size text-danger"></label>
                    </div> -->
                    <div class="form-group">
                        <label>前台欄位名稱</label>
                        <input class="form-control" name="page_name" value="{{ $company->page_name }}">
                    </div>
                    <div class="form-group">
                        <label>公司名稱</label>
                        <input class="form-control" name="title" value="{{ $company->title }}">
                    </div>
                    <div class="form-group">
                        <label>Facebook粉絲團</label>
                        <input class="form-control" name="fb_link" value="{{ $company->fb_link }}">
                    </div>
                    <div class="form-group">
                        <label>Line帳號</label>
                        <input class="form-control" name="line_link" value="{{ $company->line_link }}">
                    </div>
                    <div class="form-group">
                        <label>電話號碼</label>
                        <input class="form-control" name="phone" value="{{ $company->phone }}">
                    </div>
                    <div class="form-group">
                        <label>頁尾顯示電話號碼</label>
                        <input class="form-control" name="shown_phone" value="{{ $company->shown_phone }}">
                    </div>
                    <div class="form-group">
                        <label>電子郵件</label>
                        <input class="form-control" type="email" name="email" value="{{ $company->email }}">
                    </div>
                </div>
                <div class="col-md-6">
                    <!-- <div class="form-group">
                        <label>中間 Banner</label><br>
                        <img class="preview" src="https://placehold.it/300x300?text=photo1.jpg" style="margin:10px 0;"><br>
                        <input type='hidden' name="banner">
                        <input style="margin:10px 0;" type='file' name="img" accept='image/*' id="banner"><br>
                        <label class="size text-danger"></label>
                    </div> -->
                    <div class="form-group">
                        <label>公司地址</label>
                        <input class="form-control" name="address" value="{{ $company->address }}">
                    </div>
                    <div class="form-group">
                        <label>地圖連結</label>
                        <input class="form-control" name="map_link" value="{{ $company->map_link }}">
                    </div>
                    <div class="form-group">
                        <label>嵌入式地圖</label>
                        <input class="form-control" name="map_iframe" value="{{ $company->map_iframe }}">
                    </div>
                    <div class="form-group">
                        <label>SEO title</label>
                        <input class="form-control" name="seo_title" value="{{ $company->seo_title }}">
                    </div>
                    <div class="form-group">
                        <label>SEO description</label>
                        <textarea class="form-control" name="seo_description" rows="5">{{ $company->seo_description }}</textarea>
                    </div>
                    <button class="btn btn-success btn-lg pull-right submit" type="submit" style="margin-top: 1em;">儲存</button>
                </div>
                
            </div>
        {{ Form::close() }}
    </div>
@endsection
@section('scripts')
    <script>
        @include('session_flash', ['flashes' => [
            'home',
        ]])
    </script>
@endsection
