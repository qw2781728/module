@extends("layout.app")
@section('header')
    <style>
        h2 .btn {
            margin:0 10px;
            color:#fff;
        }
    </style>
@endsection
@section('content') 
    <div class="page-header">
        <h2>
            <span class="text-muted font-weight-light">
                <span>&nbsp;&nbsp;&nbsp;&nbsp;典當精品</span>
            </span>
            <a href="{{ route('product.setting') }}" class="btn btn-success">基本設定</a>
            <a href="{{ route('product.create') }}" class="btn btn-primary">新增商品</a>
        </h2>
        <span id="sort"></span>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <label class="panel-title"></label>
                </div>
                <div class="panel-body btn-margins">
                    <div class="col-md-12">
                        <div class="table-responsive">
                        <table class="table table-striped" style="text-align:center;">
                            <thead>
                                <tr>
                                    <th>排序</th>
                                    <th>圖片</th>
                                    <th>名稱</th>
                                    <th>原價</th>
                                    <th>特價</th>
                                    <th>功能</th>
                                </tr>
                            </thead>
                            <tbody id="list">
                                @foreach($products as $product)
                                <tr id="{{ $product->id }}">
                                    <td><i class="sort-icon fa fa-arrows" aria-hidden="true"></i>&nbsp;&nbsp;{{ $product->sort }}</td>
                                    <td><img src="{{ $product->firstPhoto() }}" alt="" srcset="" style="width: 50px; height: 50px;"></td>   
                                    <td>{{ $product->title }}</td>
                                    <td>{{ $product->price }}</td>
                                    <td>{{ $product->special_price }}</td>
                                    <td style="text-align:center;">
                                        <a class="btn btn-sm btn-primary" href="{{ route('product.edit', $product) }}">編輯</a>
                                        &nbsp;&nbsp;&nbsp;
                                        {{ Form::open(['method' => 'DELETE', 'route' => ['product.delete', $product], 'style' => 'display: inline;']) }}
                                        <button class="btn btn-sm btn-danger" type="submit">刪除</button>
                                        {{ Form::close() }}
                                    </td> 
                                </tr> 
                                @endforeach
                            </tbody>
                        </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        @include('session_flash', ['flashes' => [
            'product',
        ]])
        $(document).ready(function() {
            setSortable("{{ route('product.sort') }}");
        });
    </script>
@endsection
