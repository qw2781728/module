@extends("layout.app")
@section('header')
    <style>
        h2 .btn {
            margin:0 10px;
            color:#fff;
        }
        .submit-reset-btns {
            position: fixed;
            bottom: 70px;
            right: 275px;
        }
    </style>
@endsection
@section('content') 
    <div class="page-header">
        <h2>
            <span class="text-muted font-weight-light">
                <span>&nbsp;&nbsp;&nbsp;&nbsp;典當精品 / 新增商品</span>
            </span>
        </h2>
    </div>
    <div class="container">
        {{ Form::open(['method' => 'POST', 'action' => 'ProductController@store']) }}
            <div class="row">
                <div class="col-md-6" style="margin:15px 0;">
                    <div class="form-group">
                        <label>名稱</label>
                        <input class="form-control" name="title">
                    </div>
                    <div class="form-group">
                        <label for="news">分類標籤（不用輸入#，每個分類用,隔開）</label>
                        <input class="form-control" name="tag">
                    </div>
                    <div class="form-group">
                        <label>型號</label>
                        <input class="form-control" name="sku">
                    </div>
                    <div class="form-group">
                        <label>原價</label>
                        <input class="form-control" name="price">
                    </div>
                    <div class="form-group">
                        <label>特價</label>
                        <input class="form-control" name="special_price">
                    </div>
                    <div class="form-group">
                        <label>數量</label>
                        <input class="form-control" name="quantity">
                    </div>
                    <div class="form-group">
                        <label>SEO Title</label>
                        <input class="form-control" name="seo_title">
                    </div>
                </div>
                <div class="col-md-6" style="margin:15px 0;">
                    <div class="form-group">
                        <label>SEO Description</label>
                        <textarea style="resize: none;" class="form-control" name="seo_description" rows="5"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="">內容</label>
                        <input type="hidden" name="content">
                        <textarea id="product" class="form-control"></textarea>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label>圖片</label><br>
                        <div class="inline" id="image_box">
                            <img class="preview" src="https://placehold.it/300x300?text=photo1.jpg">
                        </div><br>
                        <input type='file' class="multiple_upload" accept='image/*' multiple>
                        <input type='hidden' name="images">
                    </div>
                </div>
                <div class="row submit-reset-btns">
                    <button type="submit" class="btn btn-lg btn-success m-r-1 submit pull-right">儲存</button>
                </div>
            </div>
        {{ Form::close() }}
    </div>
@endsection
@section('scripts')
    <script>
        var images = [];
        @include('session_flash', ['flashes' => [
            'product',
        ]])
        $(document).ready(function() {
            setTextArea('product');
            $(".submit").click(function(e) {
                e.preventDefault();
                content = $("#product").summernote('code');
                $("input[name='content']").val(content);
                if (images.length > 0) $("input[name='images']").val(JSON.stringify(images));
                $('form').submit();
            });
        });
    </script>
@endsection
