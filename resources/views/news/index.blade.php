@extends("layout.app")
@section('header')
    <style>
        h2 .btn {
            margin:0 10px;
            color:#fff;
        }
    </style>
@endsection
@section('content') 
    <div class="page-header">
        <h2>
            <span class="text-muted font-weight-light">
                <span>&nbsp;&nbsp;&nbsp;&nbsp;最新消息</span>
            </span>
            <a href="{{ route('news.setting') }}" class="btn btn-success">基本設定</a>
            <a href="{{ route('news.create') }}" class="btn btn-primary">新增文章</a>
        </h2>
        <span id="sort"></span>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <label class="panel-title"></label>
                </div>
                <div class="panel-body btn-margins">
                    <div class="col-md-12">
                        <div class="table-responsive">
                        <table class="table table-striped" style="text-align:center;">
                            <thead>
                                <tr>
                                    <th>排序</th>
                                    <th>標題</th>
                                    <th>觀看次數</th>
                                    <th>功能</th>
                                </tr>
                            </thead>
                            <tbody id="list">
                                @foreach($news as $data)
                                <tr id="{{ $data->id }}">
                                    <td><i class="sort-icon fa fa-arrows" aria-hidden="true"></i>&nbsp;&nbsp;{{ $data->sort }}</td>
                                    <td>{{ $data->title }}</td>
                                    <td>{{ $data->times }}</td>
                                    <td style="text-align:center;">
                                        <a class="btn btn-sm btn-primary" href="{{ route('news.edit', $data) }}">編輯</a>
                                        &nbsp;&nbsp;&nbsp;
                                        {{ Form::open(['method' => 'DELETE', 'route' => ['news.delete', $data], 'style' => 'display: inline;']) }}
                                        <button class="btn btn-sm btn-danger" type="submit">刪除</button>
                                        {{ Form::close() }}
                                    </td> 
                                </tr> 
                                @endforeach
                            </tbody>
                        </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        @include('session_flash', ['flashes' => [
            'news',
        ]])
        $(document).ready(function() {
            setSortable("{{ route('news.sort') }}");
        });
    </script>
@endsection
