@extends("layout.app")
@section('header')
    <style>
        h2 .btn {
            margin:0 10px;
            color:#fff;
        }
    </style>
@endsection
@section('content') 
    <div class="page-header">
        <h2>
            <span class="text-muted font-weight-light">
                <span>&nbsp;&nbsp;&nbsp;&nbsp;最新消息 / 新增文章</span>
            </span>
        </h2>
    </div>
    <div class="container">
        {{ Form::open(['method' => 'POST', 'action' => 'NewsController@store']) }}
            <div class="row">
                <div class="col-md-8" style="margin:15px 0;">
                    <div class="form-group">
                        <label>標題</label>
                        <input class="form-control" name="title">
                    </div>
                    <div class="form-group">
                        <label for="news">分類標籤（不用輸入#，每個分類用,隔開）</label>
                        <input class="form-control" name="tag">
                    </div>
                    <div class="form-group">
                        <label>首圖</label><br>
                        <img class="preview" src="https://placehold.it/300x300?text=photo1.jpg" style="margin:10px 0;"><br>
                        <input type='hidden' name="defaultImage">
                        <input style="margin:10px 0;" type='file' name="img" accept='image/*'><br>
                        <label class="size text-danger"></label>
                    </div>
                    <div class="form-group">
                        <label for="">內容</label>
                        <input type="hidden" name="content">
                        <textarea id="news" class="form-control"></textarea>
                    </div>
                    <button class="submit btn btn-lg btn-success pull-right" type="submit">儲存</button>
                </div>
            </div>
        {{ Form::close() }}
    </div>
@endsection
@section('scripts')
    <script>
        @include('session_flash', ['flashes' => [
            'news',
        ]])
        $(document).ready(function() {
            setTextArea('news');
            $(".submit").click(function(e) {
                e.preventDefault();
                content = $("#news").summernote('code');
                $("input[name='content']").val(content);
                $('form').submit();
            });
        });
    </script>
@endsection
