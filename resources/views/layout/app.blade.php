<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>{{ env('APP_NAME') }}</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}"/>
        <!-- Font Awesome -->
        <link rel="stylesheet" href="{{ asset('/plugins/font-awesome/css/font-awesome.min.css') }}">
        <!-- Ionicons -->
        <link rel="stylesheet" href="{{ asset('/https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css') }}">
        <!-- Theme style -->
        <link rel="stylesheet" href="{{ asset('/dist/css/adminlte.min.css') }}">
        <!-- iCheck -->
        <link rel="stylesheet" href="{{ asset('/plugins/iCheck/flat/blue.css') }}">
        <!-- Morris chart -->
        <link rel="stylesheet" href="{{ asset('/plugins/morris/morris.css') }}">
        <!-- jvectormap -->
        <link rel="stylesheet" href="{{ asset('/plugins/jvectormap/jquery-jvectormap-1.2.2.css') }}">
        <!-- Date Picker -->
        <link rel="stylesheet" href="{{ asset('/plugins/datepicker/datepicker3.css') }}">
        <!-- Daterange picker -->
        <link rel="stylesheet" href="{{ asset('/plugins/daterangepicker/daterangepicker-bs3.css') }}">
        <!-- bootstrap wysihtml5 - text editor -->
        <link rel="stylesheet" href="{{ asset('/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}">
        <!-- Google Font: Source Sans Pro -->
        <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote.css" rel="stylesheet">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/sortable/0.8.0/css/sortable-theme-bootstrap.min.css" rel="stylesheet">
        <style>
            .preview {
                max-height: 300px;
                max-width: 100%;
                margin: 10px;
            }
            .sort-icon {
                cursor: all-scroll;
            }
            .lnline {
                display: inline-block;
            }
            .delete-icon {
                position: absolute;
                cursor: pointer;
            }
            .multiple-preview {
                margin-left:35px;
                width:300px;
            }
            body {
                font-family: '微軟正黑體';
            }
        </style>
        @yield("header")
    </head>
    <body class="hold-transition sidebar-mini">
        <div class="wrapper">

            <nav class="main-header navbar navbar-expand bg-white navbar-light border-bottom">
                <ul class="navbar-nav" style="width: 100%;">
                    <li class="nav-item">
                        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fa fa-bars"></i></a>
                    </li>
                    <li class="nav-item d-none d-sm-inline-block">
                        <a href="{{ env('APP_FRONT_URL') }}" class="nav-link" target="_blank">查看網站</a>
                    </li>
                </ul>
            </nav>

            <aside class="main-sidebar sidebar-dark-primary elevation-4">
                <div class="sidebar">
                    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                        <div class="image">
                            <img src="{{ asset('/dist/img/user.png') }}" class="img-circle elevation-2" width="120" height="120" alt="User Image">
                        </div>
                        <div class="info">
                            <a href="#" class="d-block">{{ Session::get('user')->account }}</a>
                        </div>
                    </div>
                    <nav class="mt-2" style="letter-spacing: 2px;">
                        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                            @foreach(config()->get('constants.subjects') as $key => $subject)
                            <li class="nav-item has-treeview">
                                <a href="{{ $subject['link'] === '#' ? '#' : route($subject['link']) }}" class="nav-link">
                                    <i class="nav-icon fa fa-{{ $subject['icon'] }}"></i>
                                    <p>{{ $subject['title'] }}</p>
                                </a>
                            </li>
                            @endforeach
                            <li class="nav-item has-treeview">
                                <a href="#" class="nav-link" data-toggle="modal" data-target="#editPassword">
                                    <i class="nav-icon fa fa-edit"></i>
                                    <p>修改密碼</p>
                                </a>
                            </li>
                            <li class="nav-item has-treeview">
                                <a href="{{ route('logout') }}" class="nav-link">
                                    <i class="nav-icon fa fa-sign-out"></i>
                                    <p>登出帳號</p>
                                </a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </aside>

            <div class="modal fade" id="editPassword" tabindex="-1" role="dialog" aria-labelledby="contactModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header" style="height:50px;">
                            <b><h5 style="text-align:left;font-size:20px;color: #0d0d0d;" class="modal-title">修改密碼</h5></b>
                        </div>
                        {{ Form::open(['method' => 'POST', 'action' => 'Auth\ResetPasswordController@reset']) }}
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label class="col-md-3 control-label" style="color: #0d0d0d;">密碼</label>
                                            <div class="col-md-12">
                                                <input type="password" name="old_password" class="form-control" required>
                                            </div><br>
                                            <label class="col-md-3 control-label" style="color: #0d0d0d;">新密碼</label>
                                            <div class="col-md-12">
                                                <input type="password" name="new_password" class="form-control" required>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-primary">送出</button>
                                <button type="button" class="btn btn-danger" data-dismiss="modal">取消</button>
                            </div>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>

            <div class="content-wrapper">
                <div class="content-header">
                    <div class="container-fluid">
                        <div class="row mb-2">
                            <div class="col-sm-6"></div>
                            <div class="col-sm-6">
                                <ol class="breadcrumb float-sm-right"></ol>
                            </div>
                        </div>
                    </div>
                </div>

                <section class="content">
                    @yield('content')
                </section>

            </div>

            <footer class="main-footer">
                <strong>Copyright &copy; {{ date('Y') }} <a href="https://dacn.com.tw">DACN</a>.</strong>
                All rights reserved.
                <div class="float-right d-none d-sm-inline-block"></div>
            </footer>
            <aside class="control-sidebar control-sidebar-dark"></aside>
            
        </div>

        <script src="{{ asset('/plugins/jquery/jquery.min.js') }}"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
        <script>
            $.widget.bridge('uibutton', $.ui.button)
        </script>
        <script src="{{ asset('/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
        <script src="{{ asset('/plugins/morris/morris.min.js') }}"></script>
        <script src="{{ asset('/plugins/sparkline/jquery.sparkline.min.js') }}"></script>
        <script src="{{ asset('/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
        <script src="{{ asset('/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
        <script src="{{ asset('/plugins/knob/jquery.knob.js') }}"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
        <script src="{{ asset('/plugins/daterangepicker/daterangepicker.js') }}"></script>
        <script src="{{ asset('/plugins/datepicker/bootstrap-datepicker.js') }}"></script>
        <script src="{{ asset('/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}"></script>
        <script src="{{ asset('/plugins/slimScroll/jquery.slimscroll.min.js') }}"></script>
        <script src="{{ asset('/plugins/fastclick/fastclick.js') }}"></script>
        <script src="{{ asset('/dist/js/adminlte.js') }}"></script>
        <script src="{{ asset('/dist/js/pages/dashboard.js') }}"></script>
        <script src="{{ asset('/dist/js/demo.js') }}"></script>
        <script src="https://unpkg.com/sweetalert2@7.12.11/dist/sweetalert2.all.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/sortablejs@1.6.1/Sortable.min.js"></script>
        <script src="{{ asset('/dist/js/summernote/set_summernote.js') }}"></script>
        <script src="{{ asset('/dist/js/photo.js').'?'.time() }}"></script>
        <script src="{{ asset('/dist/js/sort.js') }}"></script>
        <script>
            @include('session_flash', ['flashes' => [
                'reset',
            ]])
            $(function () {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
            });
        </script>
        @yield('scripts')
    </body>
</html>