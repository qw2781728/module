@extends("layout.app")
@section('header')
    <style>
        h2 .btn {
            margin:0 10px;
            color:#fff;
        }
        .submit-reset-btns {
            position: fixed;
            bottom: 70px;
            right: 275px;
        }
    </style>
@endsection
@section('content') 
    <div class="page-header">
        <h2>
            <span class="text-muted font-weight-light">
                <span>&nbsp;&nbsp;&nbsp;&nbsp;服務內容 / 新增項目</span>
            </span>
        </h2>
    </div>
    <div class="container">
        {{ Form::open(['method' => 'POST', 'action' => 'ServiceController@store']) }}
            <div class="row">
                <div class="col-md-12" style="margin:15px 0;">
                    <div class="form-group">
                        <label>標題</label>
                        <input class="form-control col-md-6" name="title">
                    </div>
                    <div class="form-group">
                        <label>SEO Title</label>
                        <input class="form-control col-md-6" name="seo_title">
                    </div>
                    <div class="form-group">
                        <label>SEO Description</label>
                        <textarea class="form-control col-md-6" name="seo_description" rows="5"></textarea>
                    </div>
                    <div class="form-group">
                        <label>首圖</label><br>
                        <img class="preview" src="https://placehold.it/300x300?text=photo1.jpg" style="margin:10px 0;"><br>
                        <input type='hidden' name="defaultImage">
                        <input style="margin:10px 0;" type='file' name="img" accept='image/*'><br>
                        <label class="size text-danger"></label>
                    </div>
                    <hr>
                </div>
                <div class="col-md-12" id="layout"></div>
                <div class="col-md-12" style="margin: 5px 0;">
                    <input type="hidden" name="item_contents">
                    <button style="margin:0 10px;" class="submit btn btn-success pull-right" type="submit">儲存</button>
                    <a style="color: #fff;" class="btn btn-primary pull-right" onclick="itemPlus()">新增子項目</a>
                </div>
            </div>
        {{ Form::close() }}
    </div>
@endsection
@section('scripts')
    <script src="{{ asset('/dist/js/service_item.js') }}"></script>
    <script>
        var click = 0;
        var textContents = [];
        @include('session_flash', ['flashes' => [
            'service',
        ]])
        $(document).ready(function() {
            $(".submit").click(function(e) {
                e.preventDefault();
                if (textContents.length > 0) $("input[name='item_contents']").val(JSON.stringify(textContents));
                $('form').submit();
            });
        });
    </script>
@endsection
