@extends("layout.app")
@section('header')
    <style>
        h2 .btn {
            margin:0 10px;
            color:#fff;
        }
    </style>
@endsection
@section('content') 
    <div class="page-header">
        <h2>
            <span class="text-muted font-weight-light">
                <span>&nbsp;&nbsp;&nbsp;&nbsp;服務內容 / 基本設定</span>
            </span>
        </h2>
    </div>
    <div class="container">
        {{ Form::open(['method' => 'POST', 'action' => 'ServiceController@settingStore']) }}
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-8">
                <div class="form-group">
                    <label>前台欄位名稱</label>
                    <input class="form-control" name="page_name" value="{{ $meta_setting->page_name }}">
                </div>
                <button class="btn btn-success pull-right submit" type="submit" style="margin-top: 1em;">儲存</button>
                <a href="{{ route('service') }}" class="btn btn-primary pull-right submit" style="margin-top: 1em;margin-right: 1em;">返回列表</a>
            </div>
        </div>
        {{ Form::close() }}
    </div>
@endsection
@section('scripts')
    <script>
        @include('session_flash', ['flashes' => [
            'service',
        ]])
    </script>
@endsection
