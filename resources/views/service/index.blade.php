@extends("layout.app")
@section('header')
    <style>
        h2 .btn {
            margin:0 10px;
            color:#fff;
        }
    </style>
@endsection
@section('content') 
    <div class="page-header">
        <h2>
            <span class="text-muted font-weight-light">
                <span>&nbsp;&nbsp;&nbsp;&nbsp;服務內容</span>
            </span>
            <a href="{{ route('service.setting') }}" class="btn btn-success">基本設定</a>
            <a href="{{ route('service.create') }}" class="btn btn-primary">新增項目</a>
        </h2>
        <span id="sort"></span>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <label class="panel-title"></label>
                </div>
                <div class="panel-body btn-margins">
                    <div class="col-md-12">
                        <div class="table-responsive">
                        <table class="table table-striped" style="text-align:center;">
                            <thead>
                                <tr>
                                    <th>排序</th>
                                    <th>圖片</th>
                                    <th>名稱</th>
                                    <th>功能</th>
                                </tr>
                            </thead>
                            <tbody id="list">
                                @foreach($services as $service)
                                <tr id="{{ $service->id }}">
                                    <td><i class="sort-icon fa fa-arrows" aria-hidden="true"></i>&nbsp;&nbsp;{{ $service->sort }}</td>
                                    <td><img src="{{ $service->img }}" style="width: 50px; height: 50px;"></td>   
                                    <td>{{ $service->title }}</td>
                                    <td style="text-align:center;">
                                        <a class="btn btn-sm btn-primary" href="{{ route('service.edit', $service) }}">編輯</a>
                                        &nbsp;&nbsp;&nbsp;
                                        {{ Form::open(['method' => 'DELETE', 'route' => ['service.delete', $service], 'style' => 'display: inline;']) }}
                                        <button class="btn btn-sm btn-danger" type="submit">刪除</button>
                                        {{ Form::close() }}
                                    </td> 
                                </tr> 
                                @endforeach
                            </tbody>
                        </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        @include('session_flash', ['flashes' => [
            'service',
        ]])
        $(document).ready(function() {
            setSortable("{{ route('service.sort') }}");
        });
    </script>
@endsection
