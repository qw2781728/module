@extends("layout.app")
@section('header')
    <style>
        h2 .btn {
            margin:0 10px;
            color:#fff;
        }
    </style>
@endsection
@section('content') 
    <div class="page-header">
        <h2>
            <span class="text-muted font-weight-light">
                <span>&nbsp;&nbsp;&nbsp;&nbsp;客戶分享</span>
            </span>
            <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#customerModal">新增案例</a>
        </h2>
        <div class="modal fade" id="customerModal" tabindex="-1" role="dialog" aria-labelledby="contactModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header" style="height:50px;">
                        <b><h5 style="text-align:left;font-size:20px;color: #0d0d0d;" class="modal-title">客戶分享</h5></b>
                    </div>
                    {{ Form::open(['method' => 'POST', 'action' => 'CustomerController@store']) }}
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>標題</label>
                                    <input name="title" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>內容</label>
                                    <textarea name="content" class="form-control" rows="8" style="resize: none;"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">送出</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">取消</button>
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
        <div class="modal fade" id="customerEditModal" tabindex="-1" role="dialog" aria-labelledby="contactModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header" style="height:50px;">
                        <b><h5 style="text-align:left;font-size:20px;color: #0d0d0d;" class="modal-title">客戶分享</h5></b>
                    </div>
                    {{ Form::open(['method' => 'PATCH', 'id' => 'edit_form']) }}
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>標題</label>
                                    <input name="title" class="form-control" id="edit_title">
                                </div>
                                <div class="form-group">
                                    <label>內容</label>
                                    <textarea name="content" class="form-control" rows="8" style="resize: none;" id="edit_content"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">送出</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">取消</button>
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
        <span id="sort"></span>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <label class="panel-title"></label>
                </div>
                <div class="panel-body btn-margins">
                    <div class="col-md-12">
                        <div class="table-responsive">
                        <table class="table table-striped" style="text-align:center;">
                            <thead>
                                <tr>
                                    <th>排序</th>
                                    <th>標題</th>
                                    <th>功能</th>
                                </tr>
                            </thead>
                            <tbody id="list">
                                @foreach($customers as $customer)
                                <tr id="{{ $customer->id }}">
                                    <td><i class="sort-icon fa fa-arrows" aria-hidden="true"></i>&nbsp;&nbsp;{{ $customer->sort }}</td>
                                    <td>{{ $customer->title }}</td>
                                    <td style="text-align:center;">
                                        <a class="btn btn-sm btn-primary" style="color: #fff;" herf="javascript:;" onclick="openEditModal('{{ $customer->title }}', '{{ $customer->content }}', '{{ route('customer.update', $customer) }}')">編輯</a>
                                        &nbsp;&nbsp;&nbsp;
                                        {{ Form::open(['method' => 'DELETE', 'route' => ['customer.delete', $customer], 'style' => 'display: inline;']) }}
                                        <button class="btn btn-sm btn-danger" type="submit">刪除</button>
                                        {{ Form::close() }}
                                    </td> 
                                </tr> 
                                @endforeach
                            </tbody>
                        </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        @include('session_flash', ['flashes' => [
            'customer',
        ]])
        $(document).ready(function() {
            setSortable("{{ route('customer.sort') }}");
        });

        function openEditModal(title, content, url) {
            $('#edit_form').attr('action', url);
            $('#edit_title').val(title);
            $('#edit_content').text(content);
            $('#customerEditModal').modal('show');
        }
    </script>
@endsection
