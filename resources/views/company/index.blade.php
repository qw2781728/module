@extends("layout.app")
@section('content') 
    <div class="page-header">
        <h2>
            <span class="text-muted font-weight-light">
                <span>&nbsp;&nbsp;&nbsp;&nbsp;公司簡介</span>
            </span>
        </h2>
    </div>
    <div class="container">
        {{ Form::open(['method' => 'POST', 'action' => 'CompanyController@store']) }}
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>前台欄位名稱</label>
                        <input class="form-control" name="page_name" value="{{ $company->page_name }}">
                    </div>
                    <div class="form-group">
                        <label>SEO title</label>
                        <input class="form-control" name="seo_title" value="{{ $company->seo_title }}">
                    </div>
                    <div class="form-group">
                        <label>SEO description</label>
                        <textarea class="form-control" name="seo_description" rows="9">{{ $company->seo_description }}</textarea>
                    </div>
                </div>
                <div class="col-md-8" style="margin-bottom: 1em;">
                    <label>公司簡介</label>
                    <textarea id="description" class="form-control"></textarea>
                    <input type="hidden" name="description" value="{{ $company->description }}">
                    <button class="btn btn-success btn-lg pull-right submit" type="submit" style="margin-top: 1em;">儲存</button>
                </div>
            </div>
        {{ Form::close() }}
    </div>
@endsection
@section('scripts')
    <script>
        @include('session_flash', ['flashes' => [
            'company',
        ]])
        $(document).ready(function() {
            setTextArea('description');
            $("#description").summernote('code', `{!! $company->description !!}`);
            $(".submit").click(function(e) {
                e.preventDefault();
                description = $("#description").summernote('code');
                $("input[name='description']").val(description);
                $('form').submit();
            });
        });
    </script>
@endsection
