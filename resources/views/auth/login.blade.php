<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{{ env('APP_NAME') }}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" href="dist/css/adminlte.min.css">
    <link rel="stylesheet" href="plugins/iCheck/square/blue.css">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="hold-transition login-page">
    <div class="login-box">
        <div class="login-logo">
            <a href="#"><b>{{ env('APP_NAME') }}</b>管理後台</a>
        </div>
        <div class="card">
            <div class="card-body login-card-body">
                <p class="login-box-msg">請輸入你的帳號與密碼</p>
                {{ Form::open(['method' => 'POST', 'action' => 'Auth\LoginController@login']) }}
                <div class="form-group has-feedback">
                    <span class="fa fa-envelope form-control-feedback">&nbsp;帳號</span>
                    <input type="text" class="form-control" name="account" placeholder="Account">
                </div>
                <div class="form-group has-feedback">
                    <span class="fa fa-lock form-control-feedback">&nbsp;密碼</span>
                    <input type="password" class="form-control" name="password" placeholder="Password">
                </div>
                <div class="row">
                    <div class="col-8">
                        <div class="checkbox icheck">
                            <label>
                                <input type="checkbox">&nbsp;&nbsp;記住我
                            </label>
                        </div>
                    </div>
                    <div class="col-4">
                        <button type="submit" class="btn btn-primary btn-block">登入</button>
                    </div>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>

    <script src="plugins/jquery/jquery.min.js"></script>
    <script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="plugins/iCheck/icheck.min.js"></script>
    <script src="https://unpkg.com/sweetalert2@7.12.11/dist/sweetalert2.all.js"></script>
    <script>
        $(function () {
            $('input').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass   : 'iradio_square-blue',
                increaseArea : '20%',
            })
        })
        @include('session_flash', ['flashes' => [
            'login',
        ]])
    </script>
</body>
</html>

