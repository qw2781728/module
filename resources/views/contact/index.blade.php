@extends("layout.app")
@section('header')
    <style>
        h2 .btn {
            margin:0 10px;
            color:#fff;
        }
    </style>
@endsection
@section('content') 
    <div class="page-header">
        <h2>
            <span class="text-muted font-weight-light">
                <span>&nbsp;&nbsp;&nbsp;&nbsp;聯絡我們</span>
            </span>
        </h2>
        <div class="modal fade" id="contactModal" tabindex="-1" role="dialog" aria-labelledby="contactModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header" style="height:50px;">
                        <b><h5 style="text-align:left;font-size:20px;color: #0d0d0d;" class="modal-title" id="name"></h5></b>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label id="phone">客戶電話</label>
                                </div>
                                <div class="form-group">
                                    <label id="area">所在地區</label>
                                </div>
                                <div class="form-group">
                                    <label id="money">資金需求</label>
                                </div>
                                <div class="form-group">
                                    <label id="message">訊息內容</label>
                                </div>
                                <div class="form-group">
                                    <label id="worker">是否有勞健保</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">關閉</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <label class="panel-title"></label>
                </div>
                <div class="panel-body btn-margins">
                    <div class="col-md-12">
                        <div class="table-responsive">
                        <table class="table table-striped" style="text-align:center;">
                            <thead>
                                <tr>
                                    <th>時間</th>
                                    <th>姓名</th>
                                    <th>功能</th>
                                </tr>
                            </thead>
                            <tbody id="list">
                                @foreach($contacts as $contact)
                                <tr>
                                    <td>{{ $contact->created_at }}</td>
                                    <td>{{ $contact->name }}</td>
                                    <td><a class="btn btn-sm btn-primary" style="color: #fff;" herf="javascript:;" onclick="openModal('{{ $contact->name }}', '{{ $contact->phone }}', '{{ $contact->area }}', '{{ $contact->money }}', '{{ $contact->message }}', '{{ $contact->worker }}')">查看</a></td>
                                </tr> 
                                @endforeach
                            </tbody>
                        </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        function openModal(name, phone, area, money, message, worker) {
            $('#name').text(name);
            $('#phone').text('客戶電話：' + phone);
            $('#area').text('所在地區：' + area);
            $('#money').text('資金需求：' + money);
            $('#message').text('訊息內容：' + message);
            $('#worker').text('是否有勞健保：' + (worker == 1 ? '是' : '否'));

            $('#contactModal').modal('show');
        }
    </script>
@endsection
