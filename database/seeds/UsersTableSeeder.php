<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        
        \DB::table('users')->delete();
        
        \DB::table('users')->insert(array (
            0 => 
            array (
                'id' => 1,
                'account' => 'dacn',
                'email' => 'dacnweb@gmail.com',
                'password' => md5('00000000'),
                'remember_token' => NULL,
                'created_at' => '2019-07-17 13:44:10',
                'updated_at' => '2019-07-17 13:44:10',
            ),
        ));

    }
}