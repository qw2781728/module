<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title')->nullable();
            $table->string('logo')->nullable();
            $table->string('banner')->nullable();
            $table->text('fb_link')->nullable();
            $table->string('line_link')->nullable();
            $table->string('phone')->nullable();
            $table->string('shown_phone')->nullable();
            $table->string('email')->nullable();
            $table->string('address')->nullable();
            $table->longText('map_link')->nullable();
            $table->longText('map_iframe')->nullable();
            $table->longText('description')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companies');
    }
}
