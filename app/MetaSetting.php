<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MetaSetting extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'page_type', 'page_name', 'status', 'seo_title', 'seo_description',
    ];
}
