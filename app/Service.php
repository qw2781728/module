<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'title', 'sort', 'img', 'seo_title', 'seo_description'
    ];

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::deleting(function($service) {
            foreach ($service->serviceItems as $item) {
                $item->delete();
            }
        });
    }

    /**
     * Get the service items for the service.
     */
    public function serviceItems()
    {
        return $this->hasMany('App\ServiceItem');
    }
}
