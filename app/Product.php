<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'title', 'price', 'special_price', 'quantity', 'sku', 'tag', 'sort', 'img', 'content', 'seo_title', 'seo_description'
    ];

    /**
     * Get the first photo url after pulling it out of database.
     * 
     * @return string $photo the first photo url
     */
    public function firstPhoto()
    {
        $photos = json_decode($this->img, true);
        
        return $photo = count($photos) > 0 ? $photos[0] : "https://placehold.it/300x300?text=photo";
    }
}
