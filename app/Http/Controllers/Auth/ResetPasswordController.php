<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Foundation\Auth\ResetsPasswords;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('guest');
    }

    /**
     * Handle a reset password request to the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Http\JsonResponse
     */
    public function reset(Request $request)
    {
        // dd($request->all());
        $account = Session::get('user')->account;
        $user = User::where(['account' => $account, 'password' => md5(strval($request->old_password))])->first();
        if (!is_null($user)) {
            $user->password = md5(strval($request->new_password));
            $user->save();
            Session::put('user', $user);
            Session::flash('reset', ['type' => 'success', 'title' => '修改成功！']);
        }
        else {
            Session::flash('reset', ['type' => 'warning', 'title' => '密碼不符！']);
        }
        return redirect()->back();
    }

}
