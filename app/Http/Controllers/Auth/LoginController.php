<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('guest')->except('logout');
    }

    /**
     * Handle a login request to the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        // dd($request->all());
        $validator = $this->validator($request->all());
        if ($validator->fails()) {
            $error = $validator->errors()->all()[0];
            Session::flash('login', ['type' => 'warning', 'title' => $error]);
            return redirect()->route('login');
        }
        $user = User::where(['account' => $request->account, 'password' => md5(strval($request->password))])->first();
        if (!is_null($user)) {
            Session::put('user', $user);
            return $this->sendLoginResponse($request);
        }
        else {
            Session::flash('login', ['type' => 'warning', 'title' => '帳號與密碼不符！']);
            return $this->sendFailedLoginResponse($request);
        }
    }

    /**
     * Get a validator for an incoming login request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $rules = [
            'account' => 'required', 
            'password' => 'required',
        ];

        $messages = [
            'account.required' => '請輸入帳號！',
            'password.required' => '請輸入密碼！',
        ];

        return Validator::make($data, $rules, $messages);
    }

    /**
     * Handle a logout request to the application.
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        Session::forget('user');
        return redirect()->route('login');
    }
}
