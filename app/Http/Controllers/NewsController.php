<?php

namespace App\Http\Controllers;

use App\News;
use App\MetaSetting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class NewsController extends Controller
{
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->meta_setting = MetaSetting::firstOrCreate(['page_type' => 'news']);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $news = News::orderBy('sort')->get();

        return view('news.index', compact('news')); 
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function setting()
    {
        $meta_setting = $this->meta_setting;

        return view('news.setting', compact('meta_setting')); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function settingStore(Request $request)
    {
        $input = $request->all();
        unset($input['_token']);
        $this->meta_setting->update($input);

        Session::flash('news',  ['type' => 'success', 'title' => '儲存成功！']);
        return redirect()->route('news.setting');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('news.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return dd($request->all());
        News::create([
            'title' => $request->title,
            'tag'   => $request->tag,
            'img'   => $request->defaultImage,
            'content' => $request->content,
            'sort'    => News::max('sort') + 1,
            'times'   => 0,
        ]);
        Session::flash('news',  ['type' => 'success', 'title' => '新增成功！']);
        return redirect()->route('news');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\News  $news
     * @return \Illuminate\Http\Response
     */
    public function edit(News $news)
    {   
        return view('news.edit', compact('news'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\News  $news
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, News $news)
    {
        // return dd($request->all());
        $news->update([
            'title' => $request->title,
            'tag'   => $request->tag,
            'img'   => $request->defaultImage,
            'content' => $request->content,
        ]);
        
        Session::flash('news',  ['type' => 'success', 'title' => '修改成功！']);
        return redirect()->route('news');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function sort(Request $request)
    {
        foreach ($request->data as $id => $sort) {
            $news = News::find($id);
            $news->sort = $sort;
            $news->save();
        }
        return response()->json(array('result' => 'success'));
    }

    /**
     * 
     * Remove the specified resource from storage.
     *
     * @param  \App\News  $news
     * @return \Illuminate\Http\Response
     */
    public function destroy(News $news)
    {
        $title = $news->title;
        $news->delete();
        Session::flash('news',  ['type' => 'success', 'title' => $title.'已刪除！']);
        return redirect()->route('news');
    }

}
