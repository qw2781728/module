<?php

namespace App\Http\Controllers;

use App\Service;
use App\ServiceItem;
use App\MetaSetting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class ServiceController extends Controller
{
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->meta_setting = MetaSetting::firstOrCreate(['page_type' => 'service']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $services = Service::orderBy('sort')->get();
        return view('service.index', compact('services'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function setting()
    {
        $meta_setting = $this->meta_setting;

        return view('service.setting', compact('meta_setting')); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function settingStore(Request $request)
    {
        $input = $request->all();
        unset($input['_token']);
        $this->meta_setting->update($input);

        Session::flash('service',  ['type' => 'success', 'title' => '儲存成功！']);
        return redirect()->route('service.setting');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('service.create'); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return dd($request->all());

        $service = Service::create([
            'title' => $request->title, 
            'img'   => $request->defaultImage,
            'sort'  => Service::max('sort') + 1,
            'seo_title'  => $request->seo_title,
            'seo_description'  => $request->seo_description,
        ]);

        $contents = json_decode($request->item_contents, true);
        foreach ($request->all() as $key => $value) {
            if (preg_match('/item_title_/i', $key)) {
                $i = explode('_', $key)[2];
                ServiceItem::create([
                    'service_id' => $service->id,
                    'title'      => $value,
                    'sort'       => ServiceItem::max('sort') + 1,
                    'content'    => $contents[$i],
                ]);
            }
        }

        Session::flash('service',  ['type' => 'success', 'title' => '儲存成功！']);
        return redirect()->route('service');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function show(Service $service)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function edit(Service $service)
    {
        return view('service.edit', compact('service')); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Service $service)
    {
        $service->update([
            'title' => $request->title, 
            'img'   => $request->defaultImage,
            'seo_title'  => $request->seo_title,
            'seo_description'  => $request->seo_description,
        ]);
        $service->serviceItems()->delete();

        $contents = json_decode($request->item_contents, true);
        foreach ($request->all() as $key => $value) {
            if (preg_match('/item_title_/i', $key)) {
                $i = explode('_', $key)[2];
                ServiceItem::create([
                    'service_id' => $service->id,
                    'title'      => $value,
                    'sort'       => ServiceItem::max('sort') + 1,
                    'content'    => isset($contents[$i]) ? $contents[$i] : null,
                ]);
            }
        }

        Session::flash('service',  ['type' => 'success', 'title' => '儲存成功！']);
        return redirect()->route('service');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function sort(Request $request)
    {
        foreach ($request->data as $id => $sort) {
            $news = Service::find($id);
            $news->sort = $sort;
            $news->save();
        }
        return response()->json(array('result' => 'success'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function destroy(Service $service)
    {
        $title = $service->title;
        $service->delete();
        Session::flash('service',  ['type' => 'success', 'title' => $title.'已刪除！']);
        return redirect()->route('service');
    }
}
