<?php

namespace App\Http\Controllers;

use App\Customer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $customers = Customer::orderBy('sort')->get();
        return view('customer.index', compact('customers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return dd($request->all());
        Customer::create([
            'title'   => $request->title,
            'content' => $request->content,
            'sort'    => Customer::max('sort') + 1,
        ]);
        Session::flash('customer',  ['type' => 'success', 'title' => '儲存成功！']);
        return redirect()->route('customer');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function show(Customer $customer)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function edit(Customer $customer)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Customer $customer)
    {
        // return dd($customer);
        $customer->update([
            'title'   => $request->title,
            'content' => $request->content,
            'updated_at' => now()
        ]);
        Session::flash('customer',  ['type' => 'success', 'title' => '修改成功！']);
        return redirect()->route('customer');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function sort(Request $request)
    {
        foreach ($request->data as $id => $sort) {
            $news = Customer::find($id);
            $news->sort = $sort;
            $news->save();
        }
        return response()->json(array('result' => 'success'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Customer $customer)
    {
        $title = $customer->title;
        $customer->delete();
        Session::flash('customer',  ['type' => 'success', 'title' => $title.'已刪除！']);
        return redirect()->route('customer');
    }
}
