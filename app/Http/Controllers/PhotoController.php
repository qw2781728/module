<?php

namespace App\Http\Controllers;

use App\Helpers\PhotoHelper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class PhotoController extends Controller
{
    /**
     * Upload images for products.
     */
    public function upload(Request $request)
    {
        if (isset($request->base64)) {
            $data = $request->image;

            list($ext, $data) = explode(';', $data);
            if (strpos($ext, 'image') === false) {
                return Response::make('err');
            }

            $extension = explode('/', $ext)[1];
            if (!in_array($extension, ['png', 'jpg', 'gif', 'jpeg', 'ico'])) {
                return Response::make('invalid_type');
            }
            
            list(, $data)      = explode(',', $data);
            $file = base64_decode($data);
            // return var_dump($data);
        }
        else {
            if (!$request->hasFile('image')) {
                return Response::make('err');
            }
    
            $file = $request->file('image');
            $extension = $file->extension();
            if ($file->getSize() > 25165824) {
                return Response::make('invalid_size');
            }

            $mime = $file->getMimeType();
            if (strpos($mime, 'image') === false || !in_array($extension, ['png', 'jpg', 'gif', 'jpeg', 'ico'])) {
                return Response::make('invalid_type');
            }
        }

        $destinationPath = public_path()."/upload/image/";
        $fileName = uniqid().'.'.$extension;
        if (isset($request->base64)) {
            if (!is_dir($destinationPath)) {
                mkdir($destinationPath, 0755, true);
            }
            file_put_contents($destinationPath.$fileName, $file);
        }
        else {
            $file->move($destinationPath, $fileName);
        }
        $photo_url = "/upload/image/$fileName";
        
        $image_data = getimagesize(public_path().$photo_url);
        if (is_array($image_data)) {
            $width = $image_data[0];
            $height = $image_data[1];
        }
        
        if ($extension != 'ico') {
            $percent = 1;
            $helper = new PhotoHelper($destinationPath.$fileName, $percent);
            $helper->compressImg($destinationPath.$fileName);
        }

        return response($photo_url);
    }

    // public function delete(Request $request, $type) {
    //     $rules     = [
    //         'photo_url' => 'required|regex:/^\/upload\/'.Auth::user()->store_id.'\/'.$type.'\/\S+$/',
    //     ];

    //     $messages  = [
    //         'photo_url.required' => '缺少照片！',
    //         'photo_url.regex' => '照片格式錯誤！',
    //     ];
       
    //     $validator = Validator::make($request->all(), $rules, $messages);

    //     if ($validator->fails()) {
    //         return response()->json(array('result' => 'warning'));
    //     }

    //     $photo_url = public_path().$request->photo_url;
    //     if (file_exists($photo_url)) {
    //         unlink($photo_url);
    //         return response()->json(array('result' => 'success'));
    //     }
    //     else {
    //         return response()->json(array('result' => 'null'));
    //     }
    // }
}
