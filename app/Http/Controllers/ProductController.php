<?php

namespace App\Http\Controllers;

use App\Product;
use App\MetaSetting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class ProductController extends Controller
{

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->meta_setting = MetaSetting::firstOrCreate(['page_type' => 'product']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::orderBy('sort')->get();
        return view('product.index', compact('products'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function setting()
    {
        $meta_setting = $this->meta_setting;

        return view('product.setting', compact('meta_setting')); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function settingStore(Request $request)
    {
        $input = $request->all();
        unset($input['_token']);
        $this->meta_setting->update($input);

        Session::flash('product',  ['type' => 'success', 'title' => '儲存成功！']);
        return redirect()->route('product.setting');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('product.create'); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return dd($request->seo_description); 
        $tags = !empty($request->tag) ? $request->tag : null;
        Product::create([
            'title' => $request->title,
            'tag' => $tags,
            'sku' => $request->sku,
            'price' => $request->price,
            'special_price' => $request->special_price,
            'quantity' => $request->quantity,
            'seo_title' => $request->seo_title,
            'seo_description' => $request->seo_description,
            'sort' => Product::max('sort') + 1,
            'content' => $request->content,
            'img' => $request->images,
        ]);

        Session::flash('product',  ['type' => 'success', 'title' => '儲存成功！']);
        return redirect()->route('product');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        return view('product.edit', compact('product')); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        // return dd($request->all()); 
        $tags = !empty($request->tag) ? $request->tag : null;
        $product->update([
            'title' => $request->title,
            'tag' => $tags,
            'sku' => $request->sku,
            'price' => $request->price,
            'special_price' => $request->special_price,
            'quantity' => $request->quantity,
            'seo_title' => $request->seo_title,
            'seo_description' => $request->seo_description,
            'content' => $request->content,
            'img' => $request->images,
        ]);

        Session::flash('product',  ['type' => 'success', 'title' => '儲存成功！']);
        return redirect()->route('product');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function sort(Request $request)
    {
        foreach ($request->data as $id => $sort) {
            $news = Product::find($id);
            $news->sort = $sort;
            $news->save();
        }
        return response()->json(array('result' => 'success'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        $title = $product->title;
        $product->delete();
        Session::flash('product',  ['type' => 'success', 'title' => $title.'已刪除！']);
        return redirect()->route('product');
    }
}
