<?php

namespace App\Http\Controllers;

use App\Company;
use App\MetaSetting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class CompanyController extends Controller
{
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->company = Company::first();
        $this->meta_setting = MetaSetting::firstOrCreate(['page_type' => 'company']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $company = $this->company;
        if (is_null($company)) {
            $company = new Company;
            $company->description = '';
        }
        $company->page_name = $this->meta_setting->page_name;
        $company->seo_title = $this->meta_setting->seo_title;
        $company->seo_description = $this->meta_setting->seo_description;

        return view('company.index', compact('company'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $meta = [
            'seo_title'       => $input['seo_title'],
            'seo_description' => $input['seo_description'],
            'page_name'       => $input['page_name'],
        ];

        unset($input['seo_title']);
        unset($input['seo_description']);
        unset($input['page_name']);
        unset($input['_token']);

        if (is_null($this->company)) Company::create($input);
        else  $this->company->update($input);

        $this->meta_setting->update($meta);

        Session::flash('company', ['type' => 'success', 'title' => '儲存成功！']);
        return redirect()->route('company');
    }
}
