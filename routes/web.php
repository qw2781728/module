<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/login', function () {
    return view('auth.login');
})->name('login');

Auth::routes();

Route::group(['middleware' => 'user'], function() {

    Route::get('/logout', 'Auth\LoginController@logout')->name('logout');

    Route::resource('/', 'HomeController');
    Route::get('/', 'HomeController@index')->name('home');

    Route::resource('/company', 'CompanyController');
    Route::get('/company', 'CompanyController@index')->name('company');

    Route::resource('/news', 'NewsController');
    Route::get('/news', 'NewsController@index')->name('news');
    Route::get('/news/create', 'NewsController@create')->name('news.create');
    Route::get('/news/{news}/edit', 'NewsController@edit')->name('news.edit');
    Route::patch('/news/{news}/update', 'NewsController@update')->name('news.update');
    Route::post('/news/sort', 'NewsController@sort')->name('news.sort');
    Route::delete('/news/{news}/delete', 'NewsController@destroy')->name('news.delete');
    Route::get('/news_setting', 'NewsController@setting')->name('news.setting');
    Route::post('/news_setting', 'NewsController@settingStore')->name('news.setting.store');

    Route::resource('/customer', 'CustomerController');
    Route::get('/customer', 'CustomerController@index')->name('customer');
    Route::patch('/customer/{customer}/update', 'CustomerController@update')->name('customer.update');
    Route::post('/customer/sort', 'CustomerController@sort')->name('customer.sort');
    Route::delete('/customer/{customer}/delete', 'CustomerController@destroy')->name('customer.delete');

    Route::resource('/product', 'ProductController');
    Route::get('/product', 'ProductController@index')->name('product');
    Route::get('/product/create', 'ProductController@create')->name('product.create');
    Route::get('/product/{product}/edit', 'ProductController@edit')->name('product.edit');
    Route::patch('/product/{product}/update', 'ProductController@update')->name('product.update');
    Route::post('/product/sort', 'ProductController@sort')->name('product.sort');
    Route::delete('/product/{product}/delete', 'ProductController@destroy')->name('product.delete');
    Route::get('/product_setting', 'ProductController@setting')->name('product.setting');
    Route::post('/product_setting', 'ProductController@settingStore')->name('product.setting.store');

    Route::resource('/service', 'ServiceController');
    Route::get('/service', 'ServiceController@index')->name('service');
    Route::get('/service/create', 'ServiceController@create')->name('service.create');
    Route::get('/service/{service}/edit', 'ServiceController@edit')->name('service.edit');
    Route::patch('/service/{service}/update', 'ServiceController@update')->name('service.update');
    Route::post('/service/sort', 'ServiceController@sort')->name('service.sort');
    Route::delete('/service/{service}/delete', 'ServiceController@destroy')->name('service.delete');
    Route::get('/service_setting', 'ServiceController@setting')->name('service.setting');
    Route::post('/service_setting', 'ServiceController@settingStore')->name('service.setting.store');

    Route::resource('/contact', 'ContactController');
    Route::get('/contact', 'ContactController@index')->name('contact');
    
    Route::post('/photo/upload', 'PhotoController@upload')->name('photo.upload');
});

