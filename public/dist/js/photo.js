$("input[name='img']").change(function() {
    switch ($(this).attr('id')) {
        case 'logo':
            preview(this, 'single', 'logo');
            break;
        case 'banner':
            preview(this, 'single', 'banner');
            break;
        default:
            preview(this, 'single');
            break;
    }
});

$(".multiple_upload").change(function(){
    preview(this, 'multiple')
    swal({
        title: "圖片上傳中...",
        showConfirmButton: false,
        allowEscapeKey: false,
        allowOutsideClick: false,
        timer: 3000,
        onOpen: () => {
            swal.showLoading()
        },
        onClose: () => {
            setTimeout(() => {
                multiplePreview();
            }, 500); 
        }
    });
    
});

function preview(input, type, logoBanner = null) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        if (type === 'single') {
            var $inputPhoto;
            switch (logoBanner) {
                case 'logo':
                    $inputPhoto = $("input[name='logo']");
                    break;
                case 'banner':
                    $inputPhoto = $("input[name='banner']");
                    break;
                default:
                    $inputPhoto = $("input[name='defaultImage']");
                    break;
            }
            reader.onload = function (e) {
                input.parentElement.getElementsByClassName('preview')[0].src = e.target.result
                var formData = {
                    base64: true,
                    image: e.target.result,
                };
                $.post('/photo/upload', formData, function(res) {
                    console.log(res)
                    if (res.match("jpeg|jpg|png")) $inputPhoto.val(res);
                    else $inputPhoto.val(e.target.result);
                }).fail(function(err) {
                    console.log(err)
                    $inputPhoto.val(e.target.result);
                });
                var KB = format_float(e.total / 1024, 2);
                input.parentElement.getElementsByClassName('size')[0].innerText = "檔案大小：" + KB + " KB";
            }
            reader.readAsDataURL(input.files[0]);
        }
        else {
            for (var i = 0; i < input.files.length; i++) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $.ajax({
                        type: 'POST',
                        url: "/photo/upload",
                        data: {
                            base64: true,
                            image: e.target.result,
                        },
                        success: function(res) {
                            console.log(res)
                            if (res.match("jpeg|jpg|png")) images.push(res);
                            else images.push(e.target.result)
                        },
                        error: function (error, s, t) {
                            console.log(error, s)
                            images.push(e.target.result)
                        }
                    });
                }
                reader.readAsDataURL(input.files[i]);
            }
        }
    }
}

function format_float(num, pos) {
    var size = Math.pow(10, pos);
    return Math.round(num * size) / size;
}

function multiplePreview() {
    html = '';
    for (var i = 0; i < images.length; i++) {
        html += '<img data-index="'+ i +'" class="preview multiple-preview" src="'+ images[i] +'"><i onclick="removeImage('+ i +')" class="text-danger fa fa-2x fa-times delete-icon"></i>';
    }
    if (images.length === 0) html = '<img class="preview" src="https://placehold.it/300x300?text=photo1.jpg">';
    $("#image_box").html(html);
}

function removeImage(i) {
    images.splice(i, 1);
    multiplePreview();
}