function setSortable(url) {
    var post;
    var list = document.getElementById("list");
    new Sortable(list, {
        animation: 150,
        handle: ".sort-icon",
        onUpdate: function (event, ui) {},
        onStart: function(event, ui) {
            clearTimeout(post);
        },
        onEnd: function (event, ui) {
            var arr = $("#list").children();
            var data = {};
            for (var index = 0; index < arr.length; index++) {
                var element = arr[index];
                data[element.id] = index + 1;
            }
            str = ''
            var formData = {
                data: data,
            };
            
            post = setTimeout(function() {
                $.post(url, formData, function(res) {
                    console.log(res);
                    if (res.result === "success") {
                        setTimeout(function() {
                            window.location.reload();
                        }, 1000);
                        str +=  '<div class="row">';
                        str +=      '<div class="col-md-3"></div>';
                        str +=      '<div class="col-md-6 text-center">';
                        str +=          '<div class="alert alert-info" role="alert" style="border-radius:10px;">';
                        str +=              '<i class="fa fa-check-circle-o font-size-16" aria-hidden="true"></i>';
                        str +=              '<strong class="font-size-16">&nbsp;排序成功！</strong>';
                        str +=          '</div>';
                        str +=      '</div>';
                        str +=  '</div>';
                        $("#sort").html(str);
                    }
                    else {
                        str +=  '<div class="row">';
                        str +=      '<div class="col-md-3"></div>';
                        str +=      '<div class="col-md-6 text-center">';
                        str +=          '<div class="alert alert-danger" role="alert" style="border-radius:10px;">';
                        str +=              '<i class="fa fa-times font-size-16" aria-hidden="true"></i>';
                        str +=              '<strong class="font-size-16">&nbsp;排序失敗！</strong>';
                        str +=          '</div>';
                        str +=      '</div>';
                        str +=  '</div>';
                        $("#sort").html(str);
                        setTimeout(function() {
                            $("#sort").html('');
                        }, 1200);
                    }
                }).fail(function(err) {
                    console.log(err);
                    str +=  '<div class="row">';
                    str +=      '<div class="col-md-3"></div>';
                    str +=      '<div class="col-md-6 text-center">';
                    str +=          '<div class="alert alert-danger" role="alert" style="border-radius:10px;">';
                    str +=              '<i class="fa fa-times font-size-16" aria-hidden="true"></i>';
                    str +=              '<strong class="font-size-16">&nbsp;排序過程錯誤！</strong>';
                    str +=          '</div>';
                    str +=      '</div>';
                    str +=  '</div>';
                    $("#sort").html(str);
                    setTimeout(function() {
                        $("#sort").html('');
                    }, 1200);
                })}
            , 2000);
        }
    });
}