function itemPlus() {
    click += 1;
    html = '';
    for (var i = 1; i <= click; i++) {
        html += '<div class="col-md-4" style="float: left;">';
        html +=     '<div class="form-group">';
        html +=         '<label style="margin:10px 0;">子標題 '+ i +'</label><i onclick="itemMinus('+ i +')" class="text-danger fa fa-2x fa-times pull-right"></i>';
        html +=         '<input class="form-control" name="item_title_'+ i + '"';
        titleName = 'name="item_title_' + i + '"';
        if ($(`input[` + titleName + `]`).val() !== undefined) {
            html += `value="`+ $(`input[` + titleName + `]`).val() +`"`;
        }
        html +=         '>';
        html +=         '<label style="margin:10px 0;">子項目內容</label>';
        html +=         '<textarea id="item_content_'+ i +'" class="form-control"></textarea>';
        html +=     '</div>';
        html += '</div>';
    }
    $("#layout").html(html);
    multipleSetting();
}

function itemMinus(index) {
    click -= 1;
    textContents.splice(index, 1)
    html = '';
    for (var i = 1; i <= click; i++) {
        html += '<div class="col-md-4" style="float: left;">';
        html +=     '<div class="form-group">';
        html +=         '<label style="margin:10px 0;">子標題 '+ i +'</label><i onclick="itemMinus('+ i +')" class="text-danger fa fa-2x fa-times pull-right"></i>';
        html +=         '<input class="form-control" name="item_title_'+ i + '"';
        titleName = 'name="item_title_' + i + '"';
        if (i >= index) {
            titleName = 'name="item_title_' + (i + 1) + '"';
        }
        if ($(`input[` + titleName + `]`).val() !== undefined) {
            html += `value="`+ $(`input[` + titleName + `]`).val() +`"`;
        }
        html +=         '>';
        html +=         '<label style="margin:10px 0;">子項目內容</label>';
        html +=         '<textarea id="item_content_'+ i +'" class="form-control"></textarea>';
        html +=     '</div>';
        html += '</div>';
    }
    $("#layout").html(html);
    multipleSetting();
}

function multipleSetting() {    
    for (var i = 1; i <= click; i++) {
        content = textContents[i];
        setTextArea('item_content_' + i);
        if (content) {
            $("#item_content_" + i).summernote('code', content);
        }
    }
}

function itemsPost(mod_tab) {
    for (var i = 1; i <= click; i++) {
        titleName = 'name="item_title_' + i + '"';
        id = 0;
        if (mod_tab === 'serviceItemEdit') {
            id = arItemIds[i] ? arItemIds[i] : 0;
        }
        var formData = {
            id: id,
            sort: i,
            uuid: $("input[name='uuid']").val(),
            title: $(`input[` + titleName + `]`).val(),
            content: $("#item_content_" + i).summernote('code'),
            mod_tab: mod_tab,
        };
        $.post('../controller/MagController.php', formData, function(res) {
            console.log(res);
        }).fail(function(err) {

        });
    }
}

function itemDelete(mod_tab) {
    var formData = {
        condition: { uuid: $("input[name='uuid']").val() },
        table: 'service_item',
        mod_tab: mod_tab,
    };
    $.post('../controller/MagController.php', formData, function(res) {
        console.log(res);
    }).fail(function(err) {

    });
}