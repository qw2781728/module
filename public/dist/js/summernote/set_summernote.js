function setTextArea(id) {
	$('#' + id).summernote({
		height: 300,                 
		minHeight: null,            
		maxHeight: null,            
		focus: true ,
		callbacks: {
			onImageUpload: function (files) {
				sendFile(id, files[0]);
			},
			onChange: function(contents, $editable) {
				if (id.match(/item_content/)) {
					arId = id.split("_");
					i = arId.pop();
					i = parseInt(i);
					textContents[i] = contents;
				}
			}
		}, 
		toolbar: [
			['style', ['style', 'bold', 'italic', 'underline', 'clear']],
			['font', ['strikethrough', 'superscript', 'subscript']],
			['fontsize', ['fontsize']],
			['color', ['color']],
			['para', ['ul', 'ol', 'paragraph']],
			['height', ['height']],
			['insert', ['video', 'link', 'picture', 'hr']],
		]   
	});
}

function sendFile(id, file) {
	// console.log(file)
	if (file.size >= 2097152) {
		return alert('檔案超過 2 MB！');
	}
	var data = new FormData();
	data.append("image", file);
	$.ajax({
		data: data,
		type: "POST",
		url: "/photo/upload",
		contentType: false,
		cache: false,
		processData: false,
		success: function (data) {
			console.log(data)
			$('#' + id).summernote('editor.insertImage', data);
		},
		error: function (err, status) {
			console.log(err);
			return alert(JSON.stringify(err));
		}
	});
}