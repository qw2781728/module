<?php

return [

    /**
     * Main Subjects.
     */

    'subjects' => [

        [
            'link' => 'home',
            'title' => '首頁設定',
            'icon'  => 'cog'
        ],

        [
            'link' => 'company',
            'title' => '公司簡介',
            'icon'  => 'file'
        ],

        [
            'link' => 'news',
            'title' => '最新消息',
            'icon'  => 'bell'
        ],

        [
            'link' => 'customer',
            'title' => '客戶分享',
            'icon'  => 'address-book-o'
        ],

        [
            'link' => 'service',
            'title' => '服務內容',
            'icon'  => 'folder'
        ],

        [
            'link' => 'product',
            'title' => '典當精品',
            'icon'  => 'shopping-cart'
        ],

        [
            'link' => 'contact',
            'title' => '聯絡我們',
            'icon'  => 'envelope-open-o'
        ],

    ],
];
